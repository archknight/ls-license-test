// eslint-disable-next-line no-undef
export default {
  // syntax: "postcss-scss",
  plugins: {
    "postcss-import": {},
    "postcss-advanced-variables": {},
    "postcss-custom-properties": {},
    "postcss-color-mod-function": {},
    "postcss-utilities": {
      centerMethod: "flexbox",
    },
    "postcss-preset-mantine": {},
    "postcss-simple-vars": {},
    "postcss-preset-env": {
      stage: 2,
    },
    // cssnano: {},
  },
};
