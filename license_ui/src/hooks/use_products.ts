import { useEffect } from "react";
import { useProductsStore } from "./use_products_store";

type ProductSearchKeyword = string | undefined | null;

export function useProducts(keyword: ProductSearchKeyword) {
  useEffect(() => {
    async function fetchProducts() {
      const response = await fetch(`/api/products?keyword=${keyword}`);
      const data = await response.json();
      useProductsStore.setState((state) => (state.products = data));
    }

    fetchProducts();
  }, [keyword]);
}
