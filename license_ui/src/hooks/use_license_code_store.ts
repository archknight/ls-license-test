import { create } from "zustand";

interface LicenseCodeStore {
  licenceCode?: string;
  setLicenseCode: (code: string) => void;
}

export const useLicenseCodeStore = create<LicenseCodeStore>((set) => ({
  licenceCode: undefined,
  setLicenseCode: (code) => set({ licenceCode: code }),
}));
