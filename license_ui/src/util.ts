import cx, { ClassDictionary } from "clsx";
import { isEmpty, isNil, prop } from "ramda";

export function composite(classesDefination: ClassDictionary, ...classes: string[]) {
  /** @type {import("clsx").ClassArray} */
  const classesItems = classes
    .filter((c) => !isNil(c) && !isEmpty(c))
    .map((c) => prop(c, classesDefination) ?? c);
  return cx(...classesItems);
}
