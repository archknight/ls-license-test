export const licenseValidLength = [
  { value: 1, label: "一年" },
  { value: 2, label: "两年" },
  { value: 3, label: "三年" },
  { value: 4, label: "四年" },
  { value: 5, label: "五年" },
  { value: 6, label: "六年" },
  { value: 7, label: "七年" },
  { value: 8, label: "八年" },
  { value: 9, label: "九年" },
  { value: 10, label: "十年" },
];
