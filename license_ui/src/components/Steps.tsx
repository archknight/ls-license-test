import { List, Paper } from "@mantine/core";

export function Steps() {
  return (
    <Paper shadow="md" p="lg">
      <List>
        <List.Item>
          首先下载<a href="/api/package">netfilter.zip</a>，解压缩到任意目录。
        </List.Item>
        <List.Item>
          按照其中<em>README.pdf</em>文件中的说明完成安装。
        </List.Item>
        <List.Item>之后你就可以在本页面上生成授权码了。</List.Item>
      </List>
    </Paper>
  );
}
