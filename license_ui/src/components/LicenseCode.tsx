import { useLicenseCodeStore } from "@/hooks/use_license_code_store";
import { ActionIcon, Box, Flex, Group, Paper, Title, Tooltip } from "@mantine/core";
import { notifications } from "@mantine/notifications";
import { IconCopy } from "@tabler/icons-react";
import { isEmpty, not } from "ramda";
import classes from "./LicenseCode.module.css";

export function LicenseCode() {
  const licenseCode = useLicenseCodeStore((state) => state.licenceCode);
  const copyLicenseCode = async () => {
    if (not(isEmpty(licenseCode))) {
      await navigator.clipboard.writeText(licenseCode ?? "");
      notifications.show({
        message: "授权码已复制",
        color: "green",
      });
    }
  };

  return (
    <Paper shadow="md" p="lg" className={classes["container"]}>
      <Flex direction="column" justify="flex-start" align="stretch" gap="md" h="100%">
        <Group justify="space-between">
          <Title order={4}>授权码</Title>
          <Tooltip label="复制授权码" position="top">
            <ActionIcon size="lg" onClick={copyLicenseCode}>
              <IconCopy size={16} />
            </ActionIcon>
          </Tooltip>
        </Group>
        <Box className={classes["license-code-area"]} c="green" fz="xs">
          {licenseCode}
        </Box>
      </Flex>
    </Paper>
  );
}
