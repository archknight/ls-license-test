import { Flex } from "@mantine/core";
import classes from "./App.module.css";
import { LicenseCode } from "./components/LicenseCode";
import { LicenseForm } from "./components/LicenseForm";
import { ProductList } from "./components/ProductList";
import { Steps } from "./components/Steps";

function App() {
  return (
    <Flex direction="row" justify="space-between" align="stretch" gap="lg" h="100%">
      <Flex
        direction="column"
        align="stretch"
        justify="flex-start"
        gap="lg"
        w={450}
        className={classes["form-column"]}
      >
        <Steps />
        <LicenseForm />
        <LicenseCode />
      </Flex>
      <Flex
        direction="column"
        align="stretch"
        justify="flex-start"
        gap="lg"
        className={classes["product-column"]}
      >
        <ProductList />
      </Flex>
    </Flex>
  );
}

export default App;
