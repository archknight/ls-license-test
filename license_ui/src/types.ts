export interface LicenseInfoForm {
  licenseName: string;
  assigneeName: string;
  assigneeEmail: string;
  validLength: string;
}
