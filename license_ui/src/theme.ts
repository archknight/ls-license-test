import { createTheme } from "@mantine/core";

export const theme = createTheme({
  focusRing: "never",
  fontSmoothing: true,
  defaultRadius: "xs",
  lineHeights: {
    xs: "1.2",
    sm: "1.25",
    md: "1.35",
    lg: "1.4",
    xl: "1.5",
  },
});
