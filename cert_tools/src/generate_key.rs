use clap::Args;

#[derive(Debug, Args)]
pub struct GenerateKeyOptions {
    #[arg(short, long, help = "set certificate file version")]
    version: Option<i32>,
    #[arg(short, long, help = "set where certification file stores")]
    output_path: Option<String>,
    #[arg(short, long, help = "set certification serial number")]
    serial: Option<u32>,
    #[arg(short = 'p', long, help = "set certification validity periods")]
    validity_periods: Option<u32>,
    #[arg(help = "set certification file name")]
    name: String,
}

pub fn generate_key_file(options: GenerateKeyOptions) {
    cert_lib::generate_certificate(
        options.output_path.unwrap_or(String::from(".")).as_ref(),
        &options.name,
        4096,
        options.validity_periods.unwrap_or(365),
        options.version.unwrap_or(1),
        options.serial,
    )
    .expect("generate certificate failed");
    println!("Certificate generated.");
}
