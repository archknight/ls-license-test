use clap::{Parser, Subcommand};
use generate_key::{generate_key_file, GenerateKeyOptions};
use power_equal::{calculate_equal_result, PowerEqualResultOption};

mod generate_key;
mod power_equal;

#[derive(Debug, Parser)]
#[command(
    name = "cert-tools",
    version = "0.1",
    about = "A tool for managing license server certificates"
)]
pub struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Debug, Subcommand)]
pub enum Commands {
    #[command(about = "Generate a new certificate")]
    Generate(GenerateKeyOptions),
    #[command(about = "Calculate the equal result for power plugin")]
    CalcEqual(PowerEqualResultOption),
}

fn main() {
    let args = Cli::parse();
    match args.command {
        Commands::Generate(options) => generate_key_file(options),
        Commands::CalcEqual(options) => calculate_equal_result(options),
    }
}
