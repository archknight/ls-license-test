FROM rust:1.77-bullseye AS builder

ADD sources.list /etc/apt/
RUN apt-get update && apt-get install -y \
  libc6-dev \
  libclang-dev \
  pkg-config \
  libssl-dev
ADD crates.conf /root/.cargo/config
RUN USER=root cargo new --bin license_service
ADD ./cert_lib /cert_lib
WORKDIR /license_service
COPY ./license_server/Cargo.toml ./Cargo.toml

RUN cargo build --release && rm src/*.rs target/release/deps/license_server*

ADD ./license_server/src ./src

RUN cargo build --release

FROM debian:bullseye-slim AS deployer

ADD sources.list /etc/apt/

RUN apt-get update && \
  apt-get install -y ca-certificates tzdata && \
  rm -rf /var/lib/apt/lists/*

EXPOSE 8080

ENV TZ=Asia/Shanghai \
  APP_USER=license_usr

RUN groupadd service && \
  useradd -g service $APP_USER && \
  mkdir -p /license_service

COPY --from=builder /license_service/target/release/license_server /license_service/license_server

RUN chown -R $APP_USER:service /license_service

USER $APP_USER
WORKDIR /license_service

VOLUME ["/license_service/license.key", "/license_service/license.pem", "/license_service/netfilter.zip", "/license_service/products.json"]

CMD ["./license_server"]
