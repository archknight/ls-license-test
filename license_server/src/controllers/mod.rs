mod downloads;
mod license;
mod products;

use axum::Router;

/// 生成可迭代的转化为路由定义的控制器列表。
pub fn controllers() -> Box<dyn Iterator<Item = Box<Router>>> {
    let controllers: Vec<Box<Router>> = vec![
        Box::new(products::ProductsController::init().into()),
        Box::new(license::LicenseController::init().into()),
        Box::new(downloads::DownloadHandler::init().into()),
    ];

    Box::from(controllers.into_iter())
}
