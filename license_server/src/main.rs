#![allow(dead_code)]

use tracing::{error, info};

mod certificate;
mod controllers;
mod logging;
mod products;
mod server_routes;
mod utils;
mod vo;

#[tokio::main]
async fn main() {
    // 初始化日志系统
    logging::initialize_logging();

    // 加载产品数据
    match products::load_products().await {
        Err(e) => error!("Failed to load products: {}", e),
        Ok(_) => info!("Products loaded successfully"),
    }

    // 加载用于加密的证书
    match certificate::load_certificates("license").await {
        Err(e) => error!("Failed to load certificates: {}", e),
        Ok(_) => info!("Certificates loaded successfully"),
    }

    let main_route = server_routes::ServerMainRouter::new().registers(controllers::controllers());
    // 启动服务
    let bind_addr = String::from("0.0.0.0:8080");
    info!("Server listen on [{}]", bind_addr);
    let listener = match tokio::net::TcpListener::bind(bind_addr).await {
        Ok(listener) => listener,
        Err(e) => {
            error!("Failed to bind to port 80: {}", e);
            return;
        }
    };
    if axum::serve(listener, main_route.into_make_service())
        .await
        .is_err()
    {
        error!("Failed to start server");
    }
}
