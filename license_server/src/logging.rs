use chrono::Local;
use tracing_subscriber::{
    fmt::{self, format::Writer, time::FormatTime},
    layer::SubscriberExt,
    util::SubscriberInitExt,
    EnvFilter, Registry,
};

struct LocalTimer;

impl FormatTime for LocalTimer {
    fn format_time(&self, w: &mut Writer<'_>) -> std::fmt::Result {
        write!(w, "{}", Local::now().format("%FT%T%.3f"))
    }
}

pub fn initialize_logging() {
    let console_layer = fmt::layer()
        .pretty()
        .with_file(false)
        .with_timer(LocalTimer)
        .with_writer(std::io::stderr);

    Registry::default()
        .with(EnvFilter::new("debug"))
        .with(console_layer)
        .init();
}
