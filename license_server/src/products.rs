use std::sync::OnceLock;

use serde::{Deserialize, Serialize};
use tokio::{fs::File, io::AsyncReadExt};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Product {
    pub id: String,
    pub name: String,
    pub couple: Vec<String>,
}

static PRODUCTS: OnceLock<Vec<Product>> = OnceLock::new();

pub fn get() -> &'static Vec<Product> {
    PRODUCTS.get().unwrap()
}

pub async fn load_products() -> anyhow::Result<()> {
    let mut products_file = File::open("./products.json").await?;
    let mut contents = String::new();
    products_file.read_to_string(&mut contents).await?;
    let products: Vec<Product> = serde_json::from_str(&contents)?;
    PRODUCTS.set(products).unwrap();
    Ok(())
}
