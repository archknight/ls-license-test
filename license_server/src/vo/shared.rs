use serde::Deserialize;

#[derive(Debug, Clone, Deserialize)]
pub struct KeywordQuery {
    pub keyword: Option<String>,
}

impl KeywordQuery {
    pub fn keyword(&self) -> Option<String> {
        self.keyword.clone().and_then(crate::utils::empty_to_none)
    }
}
