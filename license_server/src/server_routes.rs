use std::ops::Deref;

use axum::{routing::IntoMakeService, Router};

/// 系统服务核心路由控制组件。
pub struct ServerMainRouter {
    main_router: Router,
}

impl ServerMainRouter {
    /// 构建新的系统服务路由组件。
    pub fn new() -> Box<ServerMainRouter> {
        Box::new(ServerMainRouter {
            main_router: Router::new(),
        })
    }

    /// 将系统路由转换为可以供绑定使用的服务。
    pub fn into_make_service(self) -> IntoMakeService<Router> {
        self.main_router.into_make_service()
    }

    /// 注册新的子路由进入系统服务路由。
    ///
    /// - `route`：要注册的子路由。
    pub fn register<R>(self, route: R) -> Box<Self>
    where
        R: Into<Router>,
    {
        Box::new(ServerMainRouter {
            main_router: self.main_router.merge(route),
        })
    }

    /// 批量注册子路由进入系统服务路由。
    ///
    /// - `routes`：要注册的子路由列表。
    pub fn registers<I, R>(self, routes: I) -> Box<Self>
    where
        R: Deref<Target = Router>,
        I: Iterator<Item = R>,
    {
        let router = routes.into_iter().fold(self.main_router, |router, route| {
            router.merge(route.clone())
        });
        Box::new(ServerMainRouter {
            main_router: router,
        })
    }
}
