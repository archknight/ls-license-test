use rand::{seq::SliceRandom, thread_rng};

pub fn empty_to_none<S: Into<String>>(s: S) -> Option<String> {
    let s = s.into();
    if s.is_empty() {
        None
    } else {
        Some(s)
    }
}

const RAND_LINCENSE_STR_SRC: &str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

pub fn generate_license_id() -> String {
    let choices = RAND_LINCENSE_STR_SRC.as_bytes();
    let mut rng = thread_rng();
    let mut code: Vec<String> = vec![];
    while code.len() < 10 {
        match choices.choose(&mut rng) {
            Some(c) => code.push(String::from_utf8(vec![*c]).unwrap()),
            None => continue,
        }
    }
    code.join("")
}
